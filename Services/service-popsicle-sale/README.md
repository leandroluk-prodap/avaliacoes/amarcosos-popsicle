# Popsicle sale service api

This file contains all the instructions for running the project in your machine.

## Table of Contents

1. [Dependencies](#dependencies)
1. [Getting Started](#getting-started)
1. [Commands](#commands)
1. [Database](#database)
1. [Diagram Model](#diagram-model)
1. [Application Structure](#application-structure)
1. [Development](#development)
1. [Testing](#testing)
1. [Lint](#lint)
1. [Swagger](#swagger)

## Dependencies

As this project is private, an .env file containing the already configured connection string is being made available; if you want to use your own bank you will need a MongoDB 3.6.9 or later bank in the cloud, like [mLab](https://mlab.com/).

## Getting Started

First, clone the project:

```bash
$ git clone https://github.com/amarcosos/Popsicle.git <my-project-name>
$ cd <my-project-name>
```

Then install dependencies and check that it works

```bash
$ yarn              # Install the npm dependencies on the package.json
$ yarn start        # Run the service locally
```

If everything works, you should see the routes available in the Swagger UI [here](http://127.0.0.1:3001/api-docs/#/).

## Commands

During execution, you will most likely depend primarily on the `yarn` commands; below are the commands at your disposal:

| `yarn <script>` | Description                                                                                            |
| --------------- | ------------------------------------------------------------------------------------------------------ |
| ``              | Install the npm dependencies on the project.                                                           |
| `start`         | Run the service locally.                                                                               |
| `start:dev`     | Run the service locally in development mode.                                                           |
| `start:debug`   | Run the service locally in debug mode.                                                                 |
| `start:prod`    | Run the creation of the `dist/main` directory production.                                              |
| `lint`          | Run eslint on the `src` and `test` directories.                                                        |
| `test`          | Run unit tests with jest locally.                                                                      |
| `test:watch`    | Run enables to specify the name or path to a file to focus on a specific set of tests.                 |
| `test:cov`      | Run inclusion a test coverage for the source files.                                                    |
| `commit`        | Run [Commitizen](https://github.com/commitizen/cz-cli) repository.                                     |
| `format`        | Run to command formats all files supported by Prettier in the current directory and its subdirectories.|

## Database

The database is in [mLab](https://mlab.com), [mongoDB](https://www.mongodb.com).

Cloud automation with MongoDB in Google Cloud.

You can connect to your database using the credentials provided at: [.env](https://github.com/amarcosos/Popsicle/tree/main/Services/service-popsicle-sale/.env#L4).

## Diagram Model

For this project, the model diagram developed for the service in question was created, you can access it [here](https://github.com/amarcosos/Popsicle/blob/main/Services/service-popsicle-sale/popsicle_sale_diagram.svg).

## Application Structure

The service structure presented in this project is grouped primarily by file type. Please note, however, that this structure is only meant to serve as a guide, it is by no means prescriptive.

```
.
├── __tests__                           # Unit tests source code
├── src                                 # Application source code
│   ├── bin                             # Files to define port to connection
│   │   └── www.debug.ts                # File to define port to connection in debug mode
│   │   └── www.ts                      # File to define port to connection in production mode
│   ├── controllers                     # Typescript classes allowing you to interact with your services
│   │   └── health.controller.ts        # Methods to health check
│   │   └── product.controller.ts       # Methods to easily handle product services
│   │   └── sale.controller.ts          # Methods to easily handle sale services
│   │   └── salesman.controller.ts      # Methods to easily handle salesman services
│   │   └── site.controller.ts          # Methods to easily handle site services
│   │   └── stock.controller.ts         # Methods to easily handle stock services
│   ├── dtos                            # Data Transfer Object
│   │   └── product.dto.ts              # Class to easily handle transfer data of product model
│   │   └── sale.dto.ts                 # Class to easily handle transfer data of sale model
│   │   └── salesman.dto.ts             # Class to easily handle transfer data of salesman model
│   │   └── site.dto.ts                 # Class to easily handle transfer data of site model
│   │   └── stock.dto.ts                # Class to easily handle transfer data of stock model
│   ├── exceptions                      # Service exception
│   │   └── request_validation.error.ts # Class to bad request error exception handling
│   ├── middleware                      # Methods middleware
│   │   └── app.middleware.ts           # Method to mediate between the operating system and applications
│   ├── models                          # Database templates
│   │   └── product.model.ts            # Class to easily handle product model data manipulation
│   │   └── sale.model.ts               # Class to easily handle sale model data manipulation
│   │   └── salesman.model.ts           # Class to easily handle salesman model data manipulation
│   │   └── site.model.ts               # Class to easily handle site model data manipulation
│   │   └── stock.model.ts              # Class to easily handle stock model data manipulation
│   ├── repositories                    # Typescript classes allowing you to interact with your models
│   │   └── product.repository.ts       # Methods to easily handle product models
│   │   └── sale.repository.ts          # Methods to easily handle sale models
│   │   └── salesman.repository.ts      # Methods to easily handle salesman models
│   │   └── site.repository.ts          # Methods to easily handle site models
│   │   └── stock.repository.ts         # Methods to easily handle stock models
│   ├── services                        # Typescript classes allowing you to interact with your services
│   │   └── product.service.ts          # Methods to easily handle product service
│   │   └── sale.service.ts             # Methods to easily handle sale service
│   │   └── salesman.service.ts         # Methods to easily handle salesman service
│   │   └── site.service.ts             # Methods to easily handle site service
│   │   └── stock.service.ts            # Methods to easily handle stock service
│   ├── utils                           # Useful methods for service
│   │   └── configurations              # Methods, constants and configuration classes
│   │       └── index.ts                # Service configuration constants
│   │   └── database                    # Methods, constants and database classes
│   │       └── index.ts                # Database configuration constants
│   │   └── filters                     # Filter exception
│   │       └── index.ts                # Class to filter exception handling
│   │   └── interceptors                # Intersections in service data traffic
│   │       └── index.ts                # Methods, constants of intersections in service data traffic
│   │   └── index                       # Unification of the useful directories
│   ├── app.module.ts                   # Nestjs service module
│   ├── app.ts                          # Create service application
│   └── production-service.ts           # Start service application
└── .eslintrc.js                        # Configuration file eslint
└── .prettierrc                         # Configuration file prettier
└── nest-cli.json                       # Configuration file nestjs
└── package.json                        # Npm package management
```

## Development

To execution locally, here are your two options:

```bash
$ yarn start           # Run your service in your terminal
$ yarn start:dev       # Run your service dev mode in your terminal
```

## Testing

To add a unit test, simply create a `*.spec.ts` file anywhere in `./__test__/`.  The Jest will run them automaticaly.
You can add objects in your data faker that will only be used in your tests.
You can run the tests in locally with the command:

```bash
$ yarn test
```

## Lint

To lint your code using eslint, just run in your terminal:

```bash
$ yarn lint
```

It will run the eslint commands on your project in locally, and display any lint error you may have in your code.

## Swagger

The API has a description of its routes and how to interact with them.
You can easily do that with the swagger package included in the starter kit.

#### Author
#### Antônio Marcos de Oliveira Souza