import * as fakerStatic from 'faker';

import { ProductDTO } from 'src/dtos/product.dto';
import { ProductType } from 'src/models/product.model';

export class ProductDTOFaker {
    private productDTO: ProductDTO;

    constructor() {
        this.productDTO = new ProductDTO();
        this.productDTO.active = true;
        this.productDTO.createdAt = new Date('2020-11-28T17:05:58.135Z');
        this.productDTO.description = fakerStatic.lorem.word();
        this.productDTO.price = fakerStatic.random.number({min:1});
        this.productDTO.promotionPrice = fakerStatic.random.number({min:1});
        this.productDTO.validityOutOfTheFreezerInHours = fakerStatic.random.number({min:1});
    }

    public withProductType(productType: ProductType) {
        this.productDTO.type = productType;
        return this;
    }

    public generate() {
        return this.productDTO;
    }
}
