import * as fakerStatic from 'faker';
import { ObjectId } from 'mongodb';

import { ProductDTO } from 'src/dtos/product.dto';
import { SaleDTO } from 'src/dtos/sale.dto';
import { SalesmanDTO } from 'src/dtos/salesman.dto';
import { SiteDTO } from 'src/dtos/site.dto';

export class SaleDTOFaker {
    private saleDTO: SaleDTO;

    constructor() {
        this.saleDTO = new SaleDTO();
        this.saleDTO._id = new ObjectId();
        this.saleDTO.dateOfSale = new Date('2020-11-28T17:05:58.135Z');
        this.saleDTO.quantity = fakerStatic.random.number({min:1});
    }

    public withProduct(productDTO: ProductDTO) {
        this.saleDTO.product = productDTO;
        return this;
    }

    public withSalesman(salesmanDTO: SalesmanDTO) {
        this.saleDTO.salesman = salesmanDTO;
        return this;
    }

    public withSite(siteDTO: SiteDTO) {
        this.saleDTO.site = siteDTO;
        return this;
    }

    public generate() {
        return this.saleDTO;
    }
}
