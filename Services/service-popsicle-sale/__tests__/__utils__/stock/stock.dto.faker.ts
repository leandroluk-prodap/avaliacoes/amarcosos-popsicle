import * as fakerStatic from 'faker';
import { ObjectId } from 'mongodb';

import { SalesmanDTO } from 'src/dtos/salesman.dto';
import { SiteDTO } from 'src/dtos/site.dto';
import { ProductDTO } from 'src/dtos/product.dto';

import {
    StockDTO,
    UpdatedByDTO
} from 'src/dtos/stock.dto';

export class UpdatedByDTOFaker {
    private updatedByDTO: UpdatedByDTO;

    constructor() {
        this.updatedByDTO = new StockDTO();
        this.updatedByDTO.date = new Date('2020-11-28T17:05:58.135Z');
        this.updatedByDTO.operationQuantity = fakerStatic.random.number({min:1});
        this.updatedByDTO.posteriorQuantity = fakerStatic.random.number({min:1});
        this.updatedByDTO.previousQuantity = fakerStatic.random.number({min:1});
        this.updatedByDTO.salesman;
    }

    public withSalesman(salesmanDTO: SalesmanDTO) {
        this.updatedByDTO.salesman = salesmanDTO;
        return this;
    }

    public generate() {
        return this.updatedByDTO;
    }
}

export class StockDTOFaker {
    private stockDTO: StockDTO;

    constructor() {
        this.stockDTO = new StockDTO();
        this.stockDTO._id = new ObjectId();
        this.stockDTO.currentQuantity = fakerStatic.random.number({min:1});
        this.stockDTO.startingQuantity = fakerStatic.random.number({min:1});
        this.stockDTO.includedBy = [];
        this.stockDTO.site;
    }

    public withIncludedBy(updatedByDTO: UpdatedByDTO) {
        this.stockDTO.includedBy.push(updatedByDTO);
        return this;
    }

    public withOutputBy(updatedByDTO: UpdatedByDTO) {
        this.stockDTO.outputBy.push(updatedByDTO);
        return this;
    }

    public withProduct(productDTO: ProductDTO) {
        this.stockDTO.product = productDTO;
        return this;
    }

    public withSite(siteDTO: SiteDTO) {
        this.stockDTO.site = siteDTO;
        return this;
    }

    public generate() {
        return this.stockDTO;
    }
}
