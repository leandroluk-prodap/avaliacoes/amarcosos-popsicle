export function mockGenerate<T>(cl: new (...args) => T) {
    const MockClass = cl as jest.Mock<T>;
    return new MockClass() as jest.Mocked<T>;
}
