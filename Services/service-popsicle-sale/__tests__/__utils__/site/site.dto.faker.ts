import * as fakerStatic from 'faker';
import { ObjectId } from 'mongodb';

import {
    SiteAddressDTO,
    SiteDTO,
    StreetAddressDTO
} from 'src/dtos/site.dto';

export class StreetAddressDTOFaker {
    private streetAddressDTO: StreetAddressDTO;

    constructor() {
        this.streetAddressDTO = new StreetAddressDTO();
        this.streetAddressDTO.complement = fakerStatic.lorem.word();
        this.streetAddressDTO.name = fakerStatic.lorem.word();
        this.streetAddressDTO.number = fakerStatic.random.number({min:1})
    }

    public generate() {
        return this.streetAddressDTO;
    }
}

export class SiteAddressDTOFaker {
    private siteAddressDTO: SiteAddressDTO;

    constructor() {
        this.siteAddressDTO = new SiteAddressDTO();
        this.siteAddressDTO.city = fakerStatic.lorem.word();
        this.siteAddressDTO.cityId = fakerStatic.random.number({min:1});
        this.siteAddressDTO.country = fakerStatic.lorem.word();
        this.siteAddressDTO.countryId = fakerStatic.random.number({min:1});
        this.siteAddressDTO.state = fakerStatic.lorem.word();
        this.siteAddressDTO.stateId = fakerStatic.random.number({min:1});
    }

    public withStreetAddress(streetAddressDTO: StreetAddressDTO) {
        this.siteAddressDTO.street = streetAddressDTO;
        return this;
    }

    public generate() {
        return this.siteAddressDTO;
    }
}

export class SiteDTOFaker {
    private siteDTO: SiteDTO;

    constructor() {
        this.siteDTO = new SiteDTO();
        this.siteDTO._id = new ObjectId();
        this.siteDTO.active = true;
        this.siteDTO.name = fakerStatic.lorem.word();
        this.siteDTO.address
    }

    public withSiteAddress(siteAddressDTO: SiteAddressDTO) {
        this.siteDTO.address = siteAddressDTO;
        return this;
    }

    public generate() {
        return this.siteDTO;
    }
}
