import * as fakerStatic from 'faker';
import { ObjectId } from 'mongodb';

import { SalesmanDTO } from 'src/dtos/salesman.dto';
import { SiteDTO } from 'src/dtos/site.dto';

export class SalesmanDTOFaker {
    private salesmanDTO: SalesmanDTO;

    constructor() {
        this.salesmanDTO = new SalesmanDTO();
        this.salesmanDTO._id = new ObjectId();
        this.salesmanDTO.active = true;
        this.salesmanDTO.cpf = fakerStatic.lorem.word(11);
        this.salesmanDTO.createdAt = new Date('2020-11-28T17:05:58.135Z');
        this.salesmanDTO.lastUpdatedAt = new Date('2020-11-28T17:05:58.135Z');
        this.salesmanDTO.name = fakerStatic.lorem.word();
        this.salesmanDTO.photo = fakerStatic.lorem.word();
        // this.salesmanDTO.site;
    }

    public withSite(siteDTO: SiteDTO) {
        this.salesmanDTO.site = siteDTO;
        return this;
    }

    public generate() {
        return this.salesmanDTO;
    }
}
