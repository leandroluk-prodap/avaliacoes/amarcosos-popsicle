import { ObjectId } from 'mongodb';

import { mockGenerate } from '__tests__/__utils__/mock-generator.util';
import { SaleDTOFaker } from '__tests__/__utils__/sale/sale.dto.faker';
import { SalesmanDTOFaker } from '__tests__/__utils__/salesman/salesman.dto.faker';
import {
    SiteAddressDTOFaker,
    SiteDTOFaker,
    StreetAddressDTOFaker
} from '__tests__/__utils__/site/site.dto.faker';
import { QueryDTO } from 'src/dtos/query.dto';
import { SalesmanService } from 'src/services/salesman.service';
import { SalesmanRepository } from 'src/repositories/salesman.repository';
import { SiteRepository } from 'src/repositories/site.repository';
import { Salesman } from 'src/models/salesman.model';
import { Site } from 'src/models/site.model';

jest.mock('src/repositories/salesman.repository');
jest.mock('src/repositories/site.repository');

describe('SalesmanService', () => {
    let salesmanService: SalesmanService;
    let saleDTOFaker: SaleDTOFaker;
    let salesmanDTOFaker: SalesmanDTOFaker;
    let siteAddressDTOFaker: SiteAddressDTOFaker;
    let siteDTOFaker: SiteDTOFaker;
    let streetAddressDTOFaker: StreetAddressDTOFaker;
    const salesmanRepository = mockGenerate(SalesmanRepository);
    const siteRepository = mockGenerate(SiteRepository);

    beforeEach(() => {
        saleDTOFaker = new SaleDTOFaker();
        salesmanDTOFaker = new SalesmanDTOFaker();
        siteAddressDTOFaker = new SiteAddressDTOFaker();
        siteDTOFaker = new SiteDTOFaker();
        streetAddressDTOFaker = new StreetAddressDTOFaker();
        salesmanService = new SalesmanService(
            salesmanRepository,
            siteRepository
        );
    });

    describe('createSalesman', () => {
        const _id = new ObjectId();
        const salesman = new Salesman();
        const site = new Site();

        it('should return saved model from salesmanDTO when salesman does not exist', async () => {
            const streetAddressDTO = streetAddressDTOFaker
                .generate();
            const siteAddressDTO = siteAddressDTOFaker
                .withStreetAddress(streetAddressDTO)
                .generate();
            const siteDTO = siteDTOFaker
                .withSiteAddress(siteAddressDTO)
                .generate();
            const salesmanDTO = salesmanDTOFaker
                .withSite(siteDTO)
                .generate();

            salesman._id = _id;
            site.address = {
                city: siteAddressDTO.city,
                cityId: siteAddressDTO.cityId,
                country: siteAddressDTO.country,
                countryId: siteAddressDTO.countryId,
                state: siteAddressDTO.state,
                stateId: siteAddressDTO.stateId,
                street: {
                    complement: streetAddressDTO.complement,
                    name: streetAddressDTO.name,
                    number: streetAddressDTO.number
                }
            };

            siteRepository.findSiteById.mockResolvedValue(site);
            salesmanRepository.createSalesman.mockResolvedValue(salesman);

            expect(await salesmanService.createSalesman(salesmanDTO)).toStrictEqual(salesman);
        });
    });

    describe('findSalesmanById', () => {
        it('should return the salesman by id', async () => {
            const salesman = new Salesman();

            salesman._id = new ObjectId();

            salesmanRepository.findSalesmanById.mockResolvedValue(salesman);

            expect(await salesmanService.findSalesmanById(salesman._id.toHexString())).toStrictEqual(salesman);
        });
    });

    describe('listSalesman', () => {
        it('should return the salesmans, ordered from the most recent date of salesman', async () => {
            const salesman = new Salesman();

            salesman._id = new ObjectId();

            salesmanRepository.listSalesman.mockResolvedValue([salesman]);

            expect(await salesmanService.listSalesman({} as QueryDTO)).toStrictEqual([salesman]);
        });

        it('should return an empty array if it finds no answers to be completed for the salesmans', async () => {
            salesmanRepository.listSalesman.mockResolvedValue([]);

            expect(await salesmanService.listSalesman({} as QueryDTO)).toStrictEqual([]);
        });
    });
})
