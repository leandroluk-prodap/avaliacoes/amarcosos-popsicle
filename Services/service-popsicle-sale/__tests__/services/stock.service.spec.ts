import { ObjectId } from 'mongodb';

import { mockGenerate } from '__tests__/__utils__/mock-generator.util';
import { ProductDTOFaker } from '__tests__/__utils__/product/product.dto.faker';
import { SalesmanDTOFaker } from '__tests__/__utils__/salesman/salesman.dto.faker';
import { SiteDTOFaker } from '__tests__/__utils__/site/site.dto.faker';
import {
    StockDTOFaker,
    UpdatedByDTOFaker
} from '__tests__/__utils__/stock/stock.dto.faker';
import { QueryDTO } from 'src/dtos/query.dto';
import { StockService } from 'src/services/stock.service';
import { ProductRepository } from 'src/repositories/product.repository';
import { SalesmanRepository } from 'src/repositories/salesman.repository';
import { SiteRepository } from 'src/repositories/site.repository';
import { StockRepository } from 'src/repositories/stock.repository';
import { Product, ProductType } from 'src/models/product.model';
import { Salesman } from 'src/models/salesman.model';
import { Site } from 'src/models/site.model';
import { Stock } from 'src/models/stock.model';

jest.mock('src/repositories/product.repository');
jest.mock('src/repositories/salesman.repository');
jest.mock('src/repositories/site.repository');
jest.mock('src/repositories/stock.repository');

describe('StockService', () => {
    let productDTOFaker: ProductDTOFaker;
    let salesmanDTOFaker: SalesmanDTOFaker;
    let siteDTOFaker: SiteDTOFaker;
    let stockDTOFaker: StockDTOFaker;
    let updatedByDTOFaker: UpdatedByDTOFaker;
    let stockService: StockService;
    const productRepository = mockGenerate(ProductRepository);
    const salesmanRepository = mockGenerate(SalesmanRepository);
    const siteRepository = mockGenerate(SiteRepository);
    const stockRepository = mockGenerate(StockRepository);

    beforeEach(() => {
        productDTOFaker = new ProductDTOFaker();
        salesmanDTOFaker = new SalesmanDTOFaker();
        siteDTOFaker = new SiteDTOFaker();
        stockDTOFaker = new StockDTOFaker();
        updatedByDTOFaker = new UpdatedByDTOFaker();
        stockService = new StockService(
            productRepository,
            salesmanRepository,
            siteRepository,
            stockRepository
        );
    });

    describe('createStock', () => {
        const _id = new ObjectId();
        const stock = new Stock();
        const product = new Product();
        const salesman = new Salesman();
        const site = new Site();

        it('should return saved model from stockDTO when stock does not exist', async () => {
            const productDTO = productDTOFaker
                .withProductType(ProductType.ICE_CREAM)
                .generate();
            const siteDTO = siteDTOFaker
                .generate();
            const salesmanDTO = salesmanDTOFaker
                .withSite(siteDTO)
                .generate();
            const updatedByDTO = updatedByDTOFaker
                .withSalesman(salesmanDTO)
                .generate();
            const stockDTO = stockDTOFaker
                .withIncludedBy(updatedByDTO)
                .withProduct(productDTO)
                .withSite(siteDTO)
                .generate();

            stock._id = _id;
            stockDTO._id = _id;
            stockDTO.product._id = _id;

            productRepository.findProductById.mockResolvedValue(product);
            salesmanRepository.findSalesmanById.mockResolvedValue(salesman);
            siteRepository.findSiteById.mockResolvedValue(site);
            stockRepository.createStock.mockResolvedValue(stock);

            expect(await stockService.createStock(stockDTO)).toStrictEqual(stock);
        });
    });

    describe('findStockById', () => {
        it('should return the stock by id', async () => {
            const stock = new Stock();

            stock._id = new ObjectId();

            stockRepository.findStockById.mockResolvedValue(stock);

            expect(await stockService.findStockById(stock._id.toHexString())).toStrictEqual(stock);
        });
    });

    describe('listStock', () => {
        it('should return the stocks, ordered from the most recent date of stock', async () => {
            const stock = new Stock();

            stock._id = new ObjectId();

            stockRepository.listStock.mockResolvedValue([stock]);

            expect(await stockService.listStock({} as QueryDTO)).toStrictEqual([stock]);
        });

        it('should return an empty array if it finds no answers to be completed for the stocks', async () => {
            stockRepository.listStock.mockResolvedValue([]);

            expect(await stockService.listStock({} as QueryDTO)).toStrictEqual([]);
        });
    });
})
