import { ObjectId } from 'mongodb';

import { mockGenerate } from '__tests__/__utils__/mock-generator.util';
import { ProductDTOFaker } from '__tests__/__utils__/product/product.dto.faker';
import { SaleDTOFaker } from '__tests__/__utils__/sale/sale.dto.faker';
import { SalesmanDTOFaker } from '__tests__/__utils__/salesman/salesman.dto.faker';
import {
    SiteAddressDTOFaker,
    SiteDTOFaker,
    StreetAddressDTOFaker
} from '__tests__/__utils__/site/site.dto.faker';
import { QueryDTO } from 'src/dtos/query.dto';
import { SaleService } from 'src/services/sale.service';
import { ProductRepository } from 'src/repositories/product.repository';
import { SaleRepository } from 'src/repositories/sale.repository';
import { SalesmanRepository } from 'src/repositories/salesman.repository';
import { SiteRepository } from 'src/repositories/site.repository';
import { Product, ProductType } from 'src/models/product.model';
import { Sale } from 'src/models/sale.model';
import { Salesman } from 'src/models/salesman.model';
import { Site } from 'src/models/site.model';

jest.mock('src/repositories/product.repository');
jest.mock('src/repositories/sale.repository');
jest.mock('src/repositories/salesman.repository');
jest.mock('src/repositories/site.repository');

describe('SaleService', () => {
    let productDTOFaker: ProductDTOFaker;
    let salesmanDTOFaker: SalesmanDTOFaker;
    let siteAddressDTOFaker: SiteAddressDTOFaker;
    let siteDTOFaker: SiteDTOFaker;
    let streetAddressDTOFaker: StreetAddressDTOFaker;
    let saleService: SaleService;
    let saleDTOFaker: SaleDTOFaker;
    const productRepository = mockGenerate(ProductRepository);
    const saleRepository = mockGenerate(SaleRepository);
    const salesmanRepository = mockGenerate(SalesmanRepository);
    const siteRepository = mockGenerate(SiteRepository);

    beforeEach(() => {
        productDTOFaker = new ProductDTOFaker();
        saleDTOFaker = new SaleDTOFaker();
        salesmanDTOFaker = new SalesmanDTOFaker();
        siteAddressDTOFaker = new SiteAddressDTOFaker();
        siteDTOFaker = new SiteDTOFaker();
        streetAddressDTOFaker = new StreetAddressDTOFaker();
        saleService = new SaleService(
            productRepository,
            saleRepository,
            salesmanRepository,
            siteRepository
        );
    });

    describe('createSale', () => {
        const _id = new ObjectId();
        const product = new Product();
        const sale = new Sale();
        const salesman = new Salesman();
        const site = new Site();

        it('should return saved model from saleDTO when sale does not exist', async () => {
            const productDTO = productDTOFaker
                .withProductType(ProductType.ICE_CREAM)
                .generate();
            const streetAddressDTO = streetAddressDTOFaker
                .generate();
            const siteAddressDTO = siteAddressDTOFaker
                .withStreetAddress(streetAddressDTO)
                .generate();
            const siteDTO = siteDTOFaker
                .withSiteAddress(siteAddressDTO)
                .generate();
            const salesmanDTO = salesmanDTOFaker
                .withSite(siteDTO)
                .generate();
            const saleDTO = saleDTOFaker
                .withProduct(productDTO)
                .withSalesman(salesmanDTO)
                .withSite(siteDTO)
                .generate();

            sale._id = _id;
            site.address = {
                city: siteAddressDTO.city,
                cityId: siteAddressDTO.cityId,
                country: siteAddressDTO.country,
                countryId: siteAddressDTO.countryId,
                state: siteAddressDTO.state,
                stateId: siteAddressDTO.stateId,
                street: {
                    complement: streetAddressDTO.complement,
                    name: streetAddressDTO.name,
                    number: streetAddressDTO.number
                }
            };

            productRepository.findProductById.mockResolvedValue(product);
            salesmanRepository.findSalesmanById.mockResolvedValue(salesman);
            siteRepository.findSiteById.mockResolvedValue(site);
            saleRepository.createSale.mockResolvedValue(sale);

            expect(await saleService.createSale(saleDTO)).toStrictEqual(sale);
        });
    });

    describe('findSaleById', () => {
        it('should return the sale by id', async () => {
            const sale = new Sale();

            sale._id = new ObjectId();

            saleRepository.findSaleById.mockResolvedValue(sale);

            expect(await saleService.findSaleById(sale._id.toHexString())).toStrictEqual(sale);
        });
    });

    describe('listSale', () => {
        it('should return the sales, ordered from the most recent date of sale', async () => {
            const sale = new Sale();

            sale._id = new ObjectId();

            saleRepository.listSale.mockResolvedValue([sale]);

            expect(await saleService.listSale({} as QueryDTO)).toStrictEqual([sale]);
        });

        it('should return an empty array if it finds no answers to be completed for the sales', async () => {
            saleRepository.listSale.mockResolvedValue([]);

            expect(await saleService.listSale({} as QueryDTO)).toStrictEqual([]);
        });
    });
})
