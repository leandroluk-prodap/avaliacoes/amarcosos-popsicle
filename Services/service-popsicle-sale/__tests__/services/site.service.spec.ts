import { ObjectId } from 'mongodb';

import { mockGenerate } from '__tests__/__utils__/mock-generator.util';
import { SiteDTOFaker } from '__tests__/__utils__/site/site.dto.faker';
import { QueryDTO } from 'src/dtos/query.dto';
import { SiteService } from 'src/services/site.service';
import { SiteRepository } from 'src/repositories/site.repository';
import { Site } from 'src/models/site.model';

jest.mock('src/repositories/site.repository');

describe('SiteService', () => {
    let siteService: SiteService;
    let siteDTOFaker: SiteDTOFaker;
    const siteRepository = mockGenerate(SiteRepository);

    beforeEach(() => {
        siteDTOFaker = new SiteDTOFaker();
        siteDTOFaker = new SiteDTOFaker();
        siteService = new SiteService(siteRepository);
    });

    describe('createSite', () => {
        const _id = new ObjectId();
        const site = new Site();

        it('should return saved model from siteDTO when site does not exist', async () => {
            const siteDTO = siteDTOFaker.generate();

            site._id = _id;
            siteDTO._id = _id;

            siteRepository.createSite.mockResolvedValue(site);

            expect(await siteService.createSite(siteDTO)).toStrictEqual(site);
        });
    });

    describe('findSiteById', () => {
        it('should return the site by id', async () => {
            const site = new Site();

            site._id = new ObjectId();

            siteRepository.findSiteById.mockResolvedValue(site);

            expect(await siteService.findSiteById(site._id.toHexString())).toStrictEqual(site);
        });
    });

    describe('listSite', () => {
        it('should return the sites, ordered from the most last updated at', async () => {
            const site = new Site();

            site._id = new ObjectId();

            siteRepository.listSite.mockResolvedValue([site]);

            expect(await siteService.listSite({} as QueryDTO)).toStrictEqual([site]);
        });

        it('should return an empty array if it finds no answers to be completed for the sites', async () => {
            siteRepository.listSite.mockResolvedValue([]);

            expect(await siteService.listSite({} as QueryDTO)).toStrictEqual([]);
        });
    });
})
