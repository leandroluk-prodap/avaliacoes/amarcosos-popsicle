import { ObjectId } from 'mongodb';

import { mockGenerate } from '__tests__/__utils__/mock-generator.util';
import { ProductDTOFaker } from '__tests__/__utils__/product/product.dto.faker';
import { QueryDTO } from 'src/dtos/query.dto';
import { ProductService } from 'src/services/product.service';
import { ProductRepository } from 'src/repositories/product.repository';
import { Product } from 'src/models/product.model';

jest.mock('src/repositories/product.repository');

describe('ProductService', () => {
    let productService: ProductService;
    let productDTOFaker: ProductDTOFaker;
    const productRepository = mockGenerate(ProductRepository);

    beforeEach(() => {
        productDTOFaker = new ProductDTOFaker();
        productService = new ProductService(
            productRepository
        );
    });

    describe('createProduct', () => {
        const _id = new ObjectId();
        const product = new Product();

        it('should return saved model from productDTO when product does not exist', async () => {
            const productDTO = productDTOFaker.generate();

            product._id = _id;
            productDTO._id = _id;

            productRepository.createProduct.mockResolvedValue(product);

            expect(await productService.createProduct(productDTO)).toStrictEqual(product);
        });
    });

    describe('findProductById', () => {
        it('should return the product by id', async () => {
            const product = new Product();

            product._id = new ObjectId();

            productRepository.findProductById.mockResolvedValue(product);

            expect(await productService.findProductById(product._id.toHexString())).toStrictEqual(product);
        });
    });

    describe('listProduct', () => {
        it('should return the products, ordered from the most last updated at', async () => {
            const product = new Product();

            product._id = new ObjectId();

            productRepository.listProduct.mockResolvedValue([product]);

            expect(await productService.listProduct({} as QueryDTO)).toStrictEqual([product]);
        });

        it('should return an empty array if it finds no answers to be completed for the products', async () => {
            productRepository.listProduct.mockResolvedValue([]);

            expect(await productService.listProduct({} as QueryDTO)).toStrictEqual([]);
        });
    });
})
