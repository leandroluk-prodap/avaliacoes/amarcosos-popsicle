import cluster from 'cluster';
import os from 'os';

export class ProductionService {
    static start(callback: () => any): void {
        if (cluster.isMaster) {
            const cpuCount = os.cpus().length;

            for (let i = 0; i < cpuCount; i++) {
                cluster.fork();
            }

            cluster.on('online', function(worker: any) {
                console.warn(`Child initialized by PID: ${worker.process.pid}`);
            });

            cluster.on('exit', function(
                worker: any,
                code: number,
                signal: any,
            ) {
                console.warn(`PID ${worker.process.pid} code: ${code} signal: ${signal}`);
                cluster.fork();
            });
        } else {
            callback();
        }
    }
}
