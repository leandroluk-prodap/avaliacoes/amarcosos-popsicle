import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { JsonResponseInterceptor } from 'src/utils/interceptors/json-response.interceptor';
import { JsonErrorFilter } from 'src/utils/filters/json-error-filter.util';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { ValidationError } from 'class-validator';
import DataDogTracer from 'dd-trace';

import { connectDB } from 'src/utils';
import { RequestValidationError } from 'src/exceptions/request_validation.error';

type SwaggerOptions = {
    title: string;
    description: string;
    version: string;
    tag: string;
};

export const appMiddleware = (app: INestApplication, swaggerOptions: SwaggerOptions) => {
    const options = new DocumentBuilder()
        .setTitle(swaggerOptions.title)
        .setDescription(swaggerOptions.description)
        .setVersion(swaggerOptions.version)
        .addTag(swaggerOptions.tag)
        .build();

    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('api-docs', app, document);

    app.useGlobalPipes(
        new ValidationPipe({
            validationError: {
                target: false,
                value: false,
            },
            exceptionFactory: (validationErrors: ValidationError[]) => {
                return new RequestValidationError(validationErrors);
            },
        }),
    );

    app.useGlobalInterceptors(new JsonResponseInterceptor());
    app.useGlobalFilters(new JsonErrorFilter());

    connectDB();

    const tracer = DataDogTracer.init({
        hostname: 'datadog-agent',
    });
    tracer.use('http', {
        service: 'service-popsicle-sale',
    });
};
