import {
    CallHandler,
    ExecutionContext,
    Injectable,
    NestInterceptor
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import config from '../configurations';

enum TypeResponse {
    Error = 'Error',
    Object = 'Object',
    Array = 'Array',
    String = 'String',
    Number = 'Number',
    Date = 'Date',
    Unknown = 'Unknown',
}

interface Meta {
    total?: number;
    skip?: number;
    limit?: number;
    totalDocs?: number;
    _id?: any;
    type: TypeResponse;
}

class JsonResponse<T> {
    constructor(public readonly data: T | T[], public readonly meta: Meta) {}
}

export const getTypeOfData = (data): TypeResponse => {
    if (data instanceof Error) {
        return TypeResponse.Error;
    } else if (data instanceof Date) {
        return TypeResponse.Date;
    } else if (Array.isArray(data)) {
        return TypeResponse.Array;
    } else if (typeof data === 'object') {
        return TypeResponse.Object;
    } else if (typeof data === 'string') {
        return TypeResponse.String;
    } else if (typeof data === 'number') {
        return TypeResponse.Number;
    } else {
        return TypeResponse.Unknown;
    }
};

const generateMetaData = (data, context: ExecutionContext): Meta => {
    const type = getTypeOfData(data);
    const req = context.getArgByIndex(0);
    const countedDocs = Reflect.getMetadata(
        'countDocsFromQuery',
        global,
        'SaleRepository',
    );

    const meta: Meta = {
        type,
    };

    if (type === TypeResponse.Array) {
        meta['total'] = data.length;
        meta['page'] = Number(req.query.page) || config.page + 1;
        meta['per'] = Number(req.query.per) || config.per;
        meta['totalDocs'] = countedDocs;
    } else if (data && data._id) {
        meta['_id'] = data._id;
    } else {
        return;
    }

    return meta;
};

@Injectable()
export class JsonResponseInterceptor<T>
    implements NestInterceptor<T, JsonResponse<T>> {
    intercept(
        context: ExecutionContext,
        next: CallHandler,
    ): Observable<JsonResponse<T>> {
        return next.handle().pipe(
            map(data => {
                return new JsonResponse<T>(
                    data,
                    generateMetaData(data, context),
                );
            }),
        );
    }
}
