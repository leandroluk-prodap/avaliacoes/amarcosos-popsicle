const config = {
    page: 0,
    per: 42,
    serverErrorResponse: {
        title: 'generic_systemErrorTitle',
        text: 'generic_systemErrorText'
    },
    sort: -1
};

export default config;
