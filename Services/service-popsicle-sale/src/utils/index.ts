import config from './configurations';
import connectDB from './database';

export {
    config,
    connectDB
};
