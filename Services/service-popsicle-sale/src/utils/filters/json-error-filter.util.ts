import {
    ArgumentsHost,
    Catch,
    ExceptionFilter,
    HttpException
} from '@nestjs/common';
import { Response } from 'express';

import { RequestValidationError } from 'src/exceptions/request_validation.error';

type ErrorResponse = {
    message: string;
    status: number;
    stack?: string | Record<string, any>;
};

@Catch()
export class JsonErrorFilter implements ExceptionFilter {
    catch(exception: Error, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();

        let errorResponse: ErrorResponse = {
            message: exception.message,
            status: 500,
            stack: exception.stack ? exception.stack : ''
        };

        if (exception instanceof RequestValidationError) {
            errorResponse = {
                message: exception.message,
                status: exception.getStatus(),
                stack: exception.errors
            };
        } else if (exception instanceof HttpException) {
            errorResponse = {
                message: exception.message,
                status: exception.getStatus(),
                stack: exception.stack ? exception.stack : ''
            };
        }

        response.status(errorResponse.status).json(errorResponse);
    }
}
