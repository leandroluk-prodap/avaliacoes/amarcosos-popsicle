#!/usr/bin/env node
'use strict';

import { bootstrap } from '../app';

if (!process.env.PORT) {
    console.error('Enviroviment variable PORT was not defined.');
    process.exit(-1);
}

const port = Number(process.env.PORT);

try {
    bootstrap(port);
} catch (error) {
    console.error(`Error starting server: ${error.message}`);
    process.exit(-1);
}
