import {
    Index,
    modelOptions,
    prop as Property
} from '@typegoose/typegoose';
import { ApiProperty } from '@nestjs/swagger';
import { ObjectId } from 'mongodb';

import { Site } from './site.model';

@Index({ createdAt: 1 })
@Index(
    {
        active: 1,
        cpf: 1
    },
    { unique: true }
)
@modelOptions({
    schemaOptions: {
        collection: 'salesmans',
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
})

export class Salesman {
    _id: ObjectId;

    @ApiProperty()
    @Property()
    active: boolean;

    @ApiProperty()
    @Property()
    cpf: string;

    @ApiProperty()
    @Property()
    createdAt: Date;

    @ApiProperty()
    @Property()
    lastUpdatedAt: Date;

    @ApiProperty()
    @Property()
    name: string;

    @ApiProperty({ required: false })
    @Property()
    photo?: string;

    @ApiProperty({ type: Site })
    @Property({ type: Site })
    site: Site;
}