import {
    Index,
    modelOptions,
    prop as Property
} from '@typegoose/typegoose';
import { ApiProperty } from '@nestjs/swagger';
import { ObjectId } from 'mongodb';

export class StreetAddress {
    _id?: false;

    @ApiProperty({ required: false })
    @Property()
    complement?: string;

    @ApiProperty()
    @Property()
    name: string;

    @ApiProperty({ required: false })
    @Property()
    number?: number;
}

export class SiteAddress {
    _id?: false;

    @ApiProperty()
    @Property()
    city: string;

    @ApiProperty()
    @Property()
    cityId: number;

    @ApiProperty()
    @Property()
    country: string;

    @ApiProperty()
    @Property()
    countryId: number;

    @ApiProperty()
    @Property()
    state: string;

    @ApiProperty()
    @Property()
    stateId: number;

    @ApiProperty({ type: StreetAddress })
    @Property({ _id: false, type: StreetAddress })
    street: StreetAddress;
}

@Index({ createdAt: 1 })
@Index(
    {
        active: 1,
        name: 1
    },
    { unique: true }
)
@Index(
    {
        active: 1,
        'address.cityId': 1,
        'address.street.complement': 1,
        'address.street.name': 1,
        'address.street.number': 1
    },
    { unique: true }
)
@modelOptions({
    schemaOptions: {
        collection: 'sites',
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
})

export class Site {
    _id: ObjectId;

    @ApiProperty()
    @Property()
    active: boolean;

    @ApiProperty({ type: SiteAddress })
    @Property({ _id: false, type: SiteAddress })
    address: SiteAddress;

    @ApiProperty()
    @Property()
    createdAt: Date;

    @ApiProperty()
    @Property()
    lastUpdatedAt: Date;

    @ApiProperty()
    @Property()
    name: string;
}