import {
    Index,
    modelOptions,
    prop as Property
} from '@typegoose/typegoose';
import { ApiProperty } from '@nestjs/swagger';
import { ObjectId } from 'mongodb';

import { Product } from './product.model';
import { Salesman } from './salesman.model';
import { Site } from './site.model';

export class UpdatedBy {
    _id: ObjectId;

    @ApiProperty()
    @Property()
    date: Date;

    @ApiProperty()
    @Property()
    operationQuantity: number;

    @ApiProperty()
    @Property()
    posteriorQuantity: number;

    @ApiProperty()
    @Property()
    previousQuantity: number;

    @ApiProperty({ type: Salesman })
    @Property({ type: Salesman })
    salesman: Salesman;
}

@Index({ lastUpdatedAt: 1 })
@Index(
    { 'product._id': 1 },
    { unique: true }
)
@modelOptions({
    schemaOptions: {
        collection: 'stocks',
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
})

export class Stock {
    _id: ObjectId;

    @ApiProperty()
    @Property()
    createdAt: Date;

    @ApiProperty()
    @Property()
    currentQuantity: number;

    @ApiProperty({ type: [UpdatedBy] })
    @Property({ type: UpdatedBy })
    includedBy: UpdatedBy[];

    @ApiProperty()
    @Property()
    lastUpdatedAt: Date;

    @ApiProperty({ type: [UpdatedBy] })
    @Property({ type: UpdatedBy })
    outputBy: UpdatedBy[];

    @ApiProperty({ type: Product })
    @Property({ type: Product })
    product: Product;

    @ApiProperty({ type: Site })
    @Property({ type: Site })
    site: Site;

    @ApiProperty()
    @Property()
    startingQuantity: number;
}
