import {
    Index,
    modelOptions,
    prop as Property
} from '@typegoose/typegoose';
import { ApiProperty } from '@nestjs/swagger';
import { ObjectId } from 'mongodb';

export enum ProductType {
    ACAI = 'Açaí',
    POPSICLE = 'Picolé',
    ICE_CREAM = 'Sorvete'
}

@Index({ createdAt: 1 })
@Index(
    {
        active: 1,
        description: 1,
        type: 1
    },
    { unique: true }
)
@modelOptions({
    schemaOptions: {
        collection: 'products',
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
})

export class Product {
    _id: ObjectId;

    @ApiProperty()
    @Property()
    active: boolean;

    @ApiProperty()
    @Property()
    createdAt: Date;

    @ApiProperty()
    @Property()
    description: string;

    @ApiProperty({ required: false, type: [String] })
    @Property({ type: String })
    images?: string[];

    @ApiProperty()
    @Property()
    lastUpdatedAt: Date;

    @ApiProperty()
    @Property()
    price: number;

    @ApiProperty()
    @Property()
    promotionPrice: number;

    @ApiProperty()
    @Property({ enum: ProductType })
    type: ProductType;

    @ApiProperty()
    @Property()
    validityOutOfTheFreezerInHours: number;
}
