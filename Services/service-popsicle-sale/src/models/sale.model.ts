import {
    Index,
    modelOptions,
    prop as Property
} from '@typegoose/typegoose';
import { ApiProperty } from '@nestjs/swagger';
import { ObjectId } from 'mongodb';

import { Product } from './product.model';
import { Salesman } from './salesman.model';
import { Site } from './site.model';

@Index({ dateOfSale: 1 })
@modelOptions({
    schemaOptions: {
        collection: 'sales',
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
})

export class Sale {
    _id: ObjectId;

    @ApiProperty()
    @Property()
    dateOfSale: Date;

    @ApiProperty({ type: Product })
    @Property({ type: Product })
    product: Product;

    @ApiProperty({ type: Salesman })
    @Property({ type: Salesman })
    salesman: Salesman;

    @ApiProperty()
    @Property()
    quantity: number;

    @ApiProperty({ type: Site })
    @Property({ type: Site })
    site: Site;
}
