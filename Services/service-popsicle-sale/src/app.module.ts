import {
    HttpModule,
    Module,
    NestModule
} from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TerminusModule } from '@nestjs/terminus';

import { HealthController } from './controllers/health.controller';
import { ProductController } from './controllers/product.controller';
import { SaleController } from './controllers/sale.controller';
import { SalesmanController } from './controllers/salesman.controller';
import { SiteController } from './controllers/site.controller';
import { StockController } from './controllers/stock.controller';
import { ProductService } from './services/product.service';
import { SaleService } from './services/sale.service';
import { SalesmanService } from './services/salesman.service';
import { SiteService } from './services/site.service';
import { StockService } from './services/stock.service';
import { ProductRepository } from './repositories/product.repository';
import { SaleRepository } from './repositories/sale.repository';
import { SalesmanRepository } from './repositories/salesman.repository';
import { SiteRepository } from './repositories/site.repository';
import { StockRepository } from './repositories/stock.repository';

@Module({
    imports: [
        ConfigModule.forRoot({}),
        HttpModule,
        TerminusModule
    ],
    controllers: [
        HealthController,
        ProductController,
        SaleController,
        SalesmanController,
        SiteController,
        StockController
    ],
    providers: [
        ProductService,
        SaleService,
        SalesmanService,
        SiteService,
        StockService,
        ProductRepository,
        SaleRepository,
        SalesmanRepository,
        SiteRepository,
        StockRepository,
        {
            provide: 'BASE_URL',
            useValue: process.env.SALE_API_HOST,
        }
    ]
})
export class AppModule implements NestModule {
    configure() {
        return;
    }
}
