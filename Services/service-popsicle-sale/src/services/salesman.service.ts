import {
    Injectable,
    UnprocessableEntityException
} from '@nestjs/common';
import { ObjectId } from 'mongodb';

import { QueryDTO } from 'src/dtos/query.dto';
import { SalesmanDTO } from 'src/dtos/salesman.dto';
import { SalesmanRepository } from 'src/repositories/salesman.repository';
import { SiteRepository } from 'src/repositories/site.repository';

@Injectable()
export class SalesmanService {
    constructor(
        private readonly salesmanRepository: SalesmanRepository,
        private readonly siteRepository: SiteRepository
    ) {}

    async createSalesman(salesmanDTO: SalesmanDTO): Promise<SalesmanDTO> {
        const siteId = new ObjectId(salesmanDTO.site._id).toHexString();
        const site = await this.siteRepository.findSiteById(siteId);

        if (!site) {
            throw new UnprocessableEntityException('Site not found');
        }

        salesmanDTO.active = true;
        salesmanDTO.createdAt = new Date();
        salesmanDTO.lastUpdatedAt = new Date();
        salesmanDTO.site = {
            _id: site._id,
            address: {
                city: site.address.city,
                cityId: site.address.cityId,
                country: site.address.country,
                countryId: site.address.countryId,
                state: site.address.state,
                stateId: site.address.stateId,
                street: {
                    complement: site.address.street.complement,
                    name: site.address.street.name,
                    number: site.address.street.number
                },
            },
            name: site.name
        };

        return this.salesmanRepository.createSalesman(salesmanDTO);
    }

    async findSalesmanById(id: string): Promise<SalesmanDTO> {
        return this.salesmanRepository.findSalesmanById(id);
    }

    async listSalesman(queryDTO: QueryDTO): Promise<SalesmanDTO[]> {
        return this.salesmanRepository.listSalesman(queryDTO);
    }
}
