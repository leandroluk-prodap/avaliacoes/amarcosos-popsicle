import { Injectable } from '@nestjs/common';

import { QueryDTO } from 'src/dtos/query.dto';
import { ProductDTO } from 'src/dtos/product.dto';
import { ProductRepository } from 'src/repositories/product.repository';

@Injectable()
export class ProductService {
    constructor(
        private readonly productRepository: ProductRepository
    ) {}

    async createProduct(productDTO: ProductDTO): Promise<ProductDTO> {
        productDTO.active = true;
        productDTO.createdAt = new Date();
        productDTO.lastUpdatedAt = new Date();

        return this.productRepository.createProduct(productDTO);
    }

    async findProductById(id: string): Promise<ProductDTO> {
        return this.productRepository.findProductById(id);
    }

    async listProduct(queryDTO: QueryDTO): Promise<ProductDTO[]> {
        return this.productRepository.listProduct(queryDTO);
    }
}
