import {
    Injectable,
    UnprocessableEntityException
} from '@nestjs/common';
import { ObjectId } from 'mongodb';

import { QueryDTO } from 'src/dtos/query.dto';
import { SaleDTO } from 'src/dtos/sale.dto';
import { ProductRepository } from 'src/repositories/product.repository';
import { SaleRepository } from 'src/repositories/sale.repository';
import { SalesmanRepository } from 'src/repositories/salesman.repository';
import { SiteRepository } from 'src/repositories/site.repository';

@Injectable()
export class SaleService {
    constructor(
        private readonly productRepository: ProductRepository,
        private readonly saleRepository: SaleRepository,
        private readonly salesmanRepository: SalesmanRepository,
        private readonly siteRepository: SiteRepository
    ) {}

    async createSale(saleDTO: SaleDTO): Promise<SaleDTO> {
        const productId = new ObjectId(saleDTO.product._id).toHexString();
        const salesmanId = new ObjectId(saleDTO.salesman._id).toHexString();
        const siteId = new ObjectId(saleDTO.site._id).toHexString();
        const product = await this.productRepository.findProductById(productId);

        if (!product) {
            throw new UnprocessableEntityException('Product not found');
        }

        const salesman = await this.salesmanRepository.findSalesmanById(salesmanId);

        if (!salesman) {
            throw new UnprocessableEntityException('Salesman not found');
        }

        const site = await this.siteRepository.findSiteById(siteId);

        if (!site) {
            throw new UnprocessableEntityException('Site not found');
        }

        saleDTO.dateOfSale = new Date();
        saleDTO.product = {
            _id: product._id,
            description: product.description,
            promotionPrice: product.promotionPrice,
            type: product.type
        };
        saleDTO.salesman = {
            _id: salesman._id,
            cpf: salesman.cpf,
            name: salesman.name
        };
        saleDTO.site = {
            _id: site._id,
            address: {
                city: site.address.city,
                cityId: site.address.cityId,
                country: site.address.country,
                countryId: site.address.countryId,
                state: site.address.state,
                stateId: site.address.stateId,
                street: {
                    complement: site.address.street.complement,
                    name: site.address.street.name,
                    number: site.address.street.number
                },
            },
            name: site.name
        };

        return this.saleRepository.createSale(saleDTO);
    }

    async findSaleById(id: string): Promise<SaleDTO> {
        return this.saleRepository.findSaleById(id);
    }

    async listSale(queryDTO: QueryDTO): Promise<SaleDTO[]> {
        return this.saleRepository.listSale(queryDTO);
    }
}
