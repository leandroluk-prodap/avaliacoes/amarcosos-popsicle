import {
    Injectable,
    UnprocessableEntityException
} from '@nestjs/common';
import { ObjectId } from 'mongodb';

import { QueryDTO } from 'src/dtos/query.dto';
import { QueryStockDTO } from 'src/dtos/query-stock.dto';
import { StockDTO } from 'src/dtos/stock.dto';
import { ProductRepository } from 'src/repositories/product.repository';
import { SalesmanRepository } from 'src/repositories/salesman.repository';
import { SiteRepository } from 'src/repositories/site.repository';
import { StockRepository } from 'src/repositories/stock.repository';

@Injectable()
export class StockService {
    constructor(
        private readonly productRepository: ProductRepository,
        private readonly salesmanRepository: SalesmanRepository,
        private readonly siteRepository: SiteRepository,
        private readonly stockRepository: StockRepository
    ) {}

    async createStock(stockDTO: StockDTO): Promise<StockDTO> {
        const productId = new ObjectId(stockDTO.product._id).toHexString();
        const salesmanId = new ObjectId(stockDTO.includedBy[0].salesman._id).toHexString();
        const siteId = new ObjectId(stockDTO.site._id).toHexString();
        const product = await this.productRepository.findProductById(productId);

        if (!product) {
            throw new UnprocessableEntityException('Product not found');
        }

        const salesman = await this.salesmanRepository.findSalesmanById(salesmanId);

        if (!salesman) {
            throw new UnprocessableEntityException('Salesman not found');
        }

        const site = await this.siteRepository.findSiteById(siteId);

        if (!site) {
            throw new UnprocessableEntityException('Site not found');
        }

        const stock = await this.stockRepository.findStockByProductId(productId);

        if (stock) {
            const operation = stockDTO.startingQuantity;
            const previous = stock.currentQuantity;
            const current = Number(previous + operation);

            stockDTO = stock;
            stockDTO.currentQuantity = current;
            stockDTO.includedBy.push({
                date: new Date(),
                operationQuantity: operation,
                posteriorQuantity: current,
                previousQuantity: previous,
                salesman: {
                    _id: salesman._id,
                    cpf: salesman.cpf,
                    name: salesman.name
                }
            });
            stockDTO.lastUpdatedAt = new Date();

            return this.stockRepository.updateStock(stockDTO);
        }

        stockDTO.createdAt = new Date();
        stockDTO.currentQuantity = stockDTO.startingQuantity;
        stockDTO.includedBy = [{
            date: new Date(),
            operationQuantity: stockDTO.startingQuantity,
            posteriorQuantity: stockDTO.startingQuantity,
            previousQuantity: 0,
            salesman: {
                _id: salesman._id,
                cpf: salesman.cpf,
                name: salesman.name
            }
        }];
        stockDTO.lastUpdatedAt = new Date();
        stockDTO.product = {
            _id: product._id,
            description: product.description,
            promotionPrice: product.promotionPrice,
            type: product.type
        };
        stockDTO.site = {
            _id: site._id,
            name: site.name
        };
        stockDTO.startingQuantity = stockDTO.startingQuantity;

        return this.stockRepository.createStock(stockDTO);
    }

    async findStockById(id: string): Promise<StockDTO> {
        return this.stockRepository.findStockById(id);
    }

    async listStock(queryDTO: QueryDTO): Promise<StockDTO[]> {
        return this.stockRepository.listStock(queryDTO);
    }

    async updateStock(queryStockDTO: QueryStockDTO): Promise<StockDTO> {
        const stockDTO: StockDTO = await this.stockRepository.findStockById(queryStockDTO.stockId);

        if (!stockDTO || stockDTO.startingQuantity === 0 || stockDTO.currentQuantity === 0) {
            throw new UnprocessableEntityException('Stock not found');
        }

        const salesman = await this.salesmanRepository.findSalesmanById(queryStockDTO.salesmanId);

        if (!salesman) {
            throw new UnprocessableEntityException('Salesman not found');
        }

        const operation = queryStockDTO.operationQuantity;
        const previous = stockDTO.currentQuantity || stockDTO.startingQuantity;
        const current = operation <= previous ? Number(previous - operation) : previous;

        stockDTO.lastUpdatedAt = new Date();
        stockDTO.currentQuantity = current;
        stockDTO.outputBy.push({
            date: new Date(),
            operationQuantity: operation,
            posteriorQuantity: current,
            previousQuantity: previous,
            salesman: {
                _id: salesman._id,
                cpf: salesman.cpf,
                name: salesman.name
            }
        });

        return this.stockRepository.updateStock(stockDTO);
    }
}
