import { Injectable } from '@nestjs/common';

import { QueryDTO } from 'src/dtos/query.dto';
import { SiteDTO } from 'src/dtos/site.dto';
import { SiteRepository } from 'src/repositories/site.repository';

@Injectable()
export class SiteService {
    constructor(
        private readonly siteRepository: SiteRepository
    ) {}

    async createSite(siteDTO: SiteDTO): Promise<SiteDTO> {
        siteDTO.active = true;
        siteDTO.createdAt = new Date();
        siteDTO.lastUpdatedAt = new Date();

        return this.siteRepository.createSite(siteDTO);
    }

    async findSiteById(id: string): Promise<SiteDTO> {
        return this.siteRepository.findSiteById(id);
    }

    async listSite(queryDTO: QueryDTO): Promise<SiteDTO[]> {
        return this.siteRepository.listSite(queryDTO);
    }
}
