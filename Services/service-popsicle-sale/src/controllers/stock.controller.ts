import {
    Body,
    Controller,
    Get,
    HttpCode,
    Param,
    Post,
    Put,
    Query
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiBody,
    ApiTags
} from '@nestjs/swagger';

import { QueryDTO } from 'src/dtos/query.dto';
import { QueryStockDTO } from 'src/dtos/query-stock.dto';
import { StockDTO } from 'src/dtos/stock.dto';
import { StockService } from '../services/stock.service';

@ApiTags('Stock')
@Controller('stock')
@ApiBearerAuth()
export class StockController {
    constructor(
        private readonly stockService: StockService
    ) {}

    @Get()
    @HttpCode(200)
    async listStock(@Query() queryDTO: QueryDTO): Promise<StockDTO[]> {
        return this.stockService.listStock(queryDTO);
    }

    @Get(':id')
    @HttpCode(200)
    async findStockById(@Param('id') id: string): Promise<StockDTO> {
        return this.stockService.findStockById(id);
    }

    @Post()
    @ApiBody({
        schema: {
            type: 'object',
            required: [
                'includedBy',
                'product',
                'site',
                'startingQuantity'
            ],
            properties: {
                includedBy: {
                    type: 'array',
                    items: {
                        required: [ 'salesman' ],
                        properties: {
                            salesman: {
                                type: 'object',
                                required: [ '_id' ],
                                properties: {
                                    _id: { type: 'string' }
                                }
                            }
                        }
                    }
                },
                product: {
                    type: 'object',
                    required: [ '_id' ],
                    properties: {
                        _id: { type: 'string' }
                    }
                },
                site: {
                    type: 'object',
                    required: [ '_id' ],
                    properties: {
                        _id: { type: 'string' }
                    }
                },
                startingQuantity: { type: 'number' }
            }
        }
    })
    @HttpCode(201)
    async createStock(@Body() stockDTO: StockDTO): Promise<StockDTO> {
        return this.stockService.createStock(stockDTO);
    }

    @Put(':id')
    @HttpCode(200)
    async updateStock(@Query() queryStockDTO: QueryStockDTO): Promise<StockDTO> {
        return this.stockService.updateStock(queryStockDTO);
    }
}
