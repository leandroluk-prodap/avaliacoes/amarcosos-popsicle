import {
    Body,
    Controller,
    Get,
    HttpCode,
    Param,
    Post,
    Query
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiBody,
    ApiTags
} from '@nestjs/swagger';

import { QueryDTO } from 'src/dtos/query.dto';
import { SiteDTO } from 'src/dtos/site.dto';
import { SiteService } from '../services/site.service';

@ApiTags('Site')
@Controller('site')
@ApiBearerAuth()
export class SiteController {
    constructor(
        private readonly siteService: SiteService
    ) {}

    @Get()
    @HttpCode(200)
    async listSite(@Query() queryDTO: QueryDTO): Promise<SiteDTO[]> {
        return this.siteService.listSite(queryDTO);
    }

    @Get(':id')
    @HttpCode(200)
    async findSiteById(@Param('id') id: string): Promise<SiteDTO> {
        return this.siteService.findSiteById(id);
    }

    @Post()
    @ApiBody({
        schema: {
            type: 'object',
            required: [
                'name'
            ],
            properties: {
                address: {
                    type: 'object',
                    required: [
                        'city',
                        'cityId',
                        'country',
                        'countryId',
                        'state',
                        'stateId'
                    ],
                    properties: {
                        city: { type: 'string' },
                        cityId: { type: 'number' },
                        country: { type: 'string' },
                        countryId: { type: 'number' },
                        state: { type: 'string' },
                        stateId: { type: 'number' },
                        street: {
                            type: 'object',
                            required: [
                                'name'
                            ],
                            properties: {
                                complement: { type: 'string' },
                                name: { type: 'string' },
                                number: { type: 'number' }
                            }
                        }
                    }
                },
                name: { type: 'string' },
            }
        }
    })
    @HttpCode(201)
    async createSite(@Body() siteDTO: SiteDTO): Promise<SiteDTO> {
        return this.siteService.createSite(siteDTO);
    }
}
