import {
    Body,
    Controller,
    Get,
    HttpCode,
    Param,
    Post,
    Query
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiBody,
    ApiTags
} from '@nestjs/swagger';

import { QueryDTO } from 'src/dtos/query.dto';
import { SaleDTO } from 'src/dtos/sale.dto';
import { SaleService } from '../services/sale.service';

@ApiTags('Sale')
@Controller('sale')
@ApiBearerAuth()
export class SaleController {
    constructor(
        private readonly saleService: SaleService
    ) {}

    @Get()
    @HttpCode(200)
    async listSale(@Query() queryDTO: QueryDTO): Promise<SaleDTO[]> {
        return this.saleService.listSale(queryDTO);
    }

    @Get(':id')
    @HttpCode(200)
    async findSaleById(@Param('id') id: string): Promise<SaleDTO> {
        return this.saleService.findSaleById(id);
    }

    @Post()
    @ApiBody({
        schema: {
            type: 'object',
            required: [
                'product',
                'quantity',
                'salesman',
                'site'
            ],
            properties: {
                product: {
                    type: 'object',
                    required: [ '_id' ],
                    properties: {
                        _id: { type: 'string' }
                    }
                },
                quantity: { type: 'number' },
                salesman: {
                    type: 'object',
                    required: [ '_id' ],
                    properties: {
                        _id: { type: 'string' }
                    }
                },
                site: {
                    type: 'object',
                    required: [ '_id' ],
                    properties: {
                        _id: { type: 'string' }
                    }
                }
            }
        }
    })
    @HttpCode(201)
    async createSale(@Body() saleDTO: SaleDTO): Promise<SaleDTO> {
        return this.saleService.createSale(saleDTO);
    }
}
