import {
    Controller,
    Get
} from '@nestjs/common';
import {
    DNSHealthIndicator,
    HealthCheck,
    HealthIndicatorResult
} from '@nestjs/terminus';
import { ApiTags } from '@nestjs/swagger';

interface HealthStatus {
    version: string;
    dns: HealthIndicatorResult;
}

@ApiTags('Health check')
@Controller('health')
export class HealthController {
    constructor(private dns: DNSHealthIndicator) {}

    @Get()
    @HealthCheck()
    async check(): Promise<HealthStatus> {
        const pingCheckResult = await this.dns.pingCheck(
            'service-popsicle-sale',
            'https://docs.nestjs.com'
        );

        return {
            version: process.env.npm_package_version,
            dns: pingCheckResult
        };
    }
}
