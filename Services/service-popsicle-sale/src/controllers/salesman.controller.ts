import {
    Body,
    Controller,
    Get,
    HttpCode,
    Param,
    Post,
    Query
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiBody,
    ApiTags
} from '@nestjs/swagger';

import { QueryDTO } from 'src/dtos/query.dto';
import { SalesmanDTO } from 'src/dtos/salesman.dto';
import { SalesmanService } from '../services/salesman.service';

@ApiTags('Salesman')
@Controller('salesman')
@ApiBearerAuth()
export class SalesmanController {
    constructor(
        private readonly salesmanService: SalesmanService
    ) {}

    @Get()
    @HttpCode(200)
    async listSalesman(@Query() queryDTO: QueryDTO): Promise<SalesmanDTO[]> {
        return this.salesmanService.listSalesman(queryDTO);
    }

    @Get(':id')
    @HttpCode(200)
    async findSalesmanById(@Param('id') id: string): Promise<SalesmanDTO> {
        return this.salesmanService.findSalesmanById(id);
    }

    @Post()
    @ApiBody({
        schema: {
            type: 'object',
            required: [
                'cpf',
                'name',
                'site'
            ],
            properties: {
                cpf: { type: 'string' },
                name: { type: 'string' },
                photo: { type: 'string' },
                site: {
                    type: 'object',
                    required: [ '_id' ],
                    properties: {
                        _id: { type: 'string' }
                    }
                }
            }
        }
    })
    @HttpCode(201)
    async createSalesman(@Body() salesmanDTO: SalesmanDTO): Promise<SalesmanDTO> {
        return this.salesmanService.createSalesman(salesmanDTO);
    }
}
