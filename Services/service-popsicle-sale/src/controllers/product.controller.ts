import {
    Body,
    Controller,
    Get,
    HttpCode,
    Param,
    Post,
    Query
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiBody,
    ApiTags
} from '@nestjs/swagger';

import { QueryDTO } from 'src/dtos/query.dto';
import { ProductDTO } from 'src/dtos/product.dto';
import { ProductService } from '../services/product.service';

@ApiTags('Product')
@Controller('product')
@ApiBearerAuth()
export class ProductController {
    constructor(
        private readonly productService: ProductService
    ) { }

    @Get()
    @HttpCode(200)
    async listProduct(@Query() queryDTO: QueryDTO): Promise<ProductDTO[]> {
        return this.productService.listProduct(queryDTO);
    }

    @Get(':id')
    @HttpCode(200)
    async findProductById(@Param('id') id: string): Promise<ProductDTO> {
        return this.productService.findProductById(id);
    }

    @Post()
    @ApiBody({
        schema: {
            type: 'object',
            required: [
                'description',
                'price',
                'promotionPrice',
                'type',
                'validityOutOfTheFreezerInHours'
            ],
            properties: {
                description: { type: 'string' },
                images: {
                    type: 'array',
                    items: {
                        type: 'string'
                    }
                },
                price: { type: 'number' },
                promotionPrice: { type: 'number' },
                type: { type: 'string' },
                validityOutOfTheFreezerInHours: { type: 'number' }
            }
        }
    })
    @HttpCode(201)
    async createProduct(@Body() productDTO: ProductDTO): Promise<ProductDTO> {
        return this.productService.createProduct(productDTO);
    }
}
