import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { getModelForClass } from '@typegoose/typegoose';

import { QueryDTO } from 'src/dtos/query.dto';
import { SaleDTO } from 'src/dtos/sale.dto';
import { Sale } from 'src/models/sale.model';
import { config } from 'src/utils';

@Injectable()
export class SaleRepository {
    private sale: Model<any>;

    constructor() {
        this.sale = getModelForClass(Sale);
    }

    async createSale(saleDTO: SaleDTO): Promise<Sale> {
        const response = await this.sale.create(saleDTO);

        return response.toJSON();
    }

    async findSaleById(id: string): Promise<Sale> {
        const response = await this.sale.findById(id);

        return response;
    }

    async listSale(queryDTO: QueryDTO): Promise<Sale[]> {
        const response = await this.sale
            .find({})
            .skip(Number((queryDTO.page - 1) * queryDTO.per) || config.page)
            .limit(Number(queryDTO.per) || config.per)
            .sort({ 'dateOfSale': Number(queryDTO.sort) || config.sort })
            .lean();

        return response as Sale[];
    }
}
