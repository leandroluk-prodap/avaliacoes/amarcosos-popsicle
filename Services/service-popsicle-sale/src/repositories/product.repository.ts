import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { getModelForClass } from '@typegoose/typegoose';

import { QueryDTO } from 'src/dtos/query.dto';
import { ProductDTO } from 'src/dtos/product.dto';
import { Product } from 'src/models/product.model';
import { config } from 'src/utils';

@Injectable()
export class ProductRepository {
    private product: Model<any>;

    constructor() {
        this.product = getModelForClass(Product);
    }

    async createProduct(ProductDTO: ProductDTO): Promise<Product> {
        const response = await this.product.create(ProductDTO);

        return response.toJSON();
    }

    async findProductById(id: string): Promise<Product> {
        const response = await this.product.findById(id);

        return response;
    }

    async listProduct(queryDTO: QueryDTO): Promise<Product[]> {
        const response = await this.product
            .find({})
            .skip(Number((queryDTO.page - 1) * queryDTO.per) || config.page)
            .limit(Number(queryDTO.per) || config.per)
            .sort({ 'lastUpdatedAt': Number(queryDTO.sort) || config.sort })
            .lean();

        return response as Product[];
    }
}
