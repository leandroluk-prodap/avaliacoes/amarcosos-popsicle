import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { getModelForClass } from '@typegoose/typegoose';

import { QueryDTO } from 'src/dtos/query.dto';
import { SalesmanDTO } from 'src/dtos/salesman.dto';
import { Salesman } from 'src/models/salesman.model';
import { config } from 'src/utils';

@Injectable()
export class SalesmanRepository {
    private salesman: Model<any>;

    constructor() {
        this.salesman = getModelForClass(Salesman);
    }

    async createSalesman(salesmanDTO: SalesmanDTO): Promise<Salesman> {
        const response = await this.salesman.create(salesmanDTO);

        return response.toJSON();
    }

    async findSalesmanById(id: string): Promise<Salesman> {
        const response = await this.salesman.findById(id);

        return response;
    }

    async listSalesman(queryDTO: QueryDTO): Promise<Salesman[]> {
        const response = await this.salesman
            .find({})
            .skip(Number((queryDTO.page - 1) * queryDTO.per) || config.page)
            .limit(Number(queryDTO.per) || config.per)
            .sort({ 'lastUpdatedAt': Number(queryDTO.sort) || config.sort })
            .lean();

        return response as Salesman[];
    }
}
