import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { getModelForClass } from '@typegoose/typegoose';

import { QueryDTO } from 'src/dtos/query.dto';
import { StockDTO } from 'src/dtos/stock.dto';
import { Stock } from 'src/models/stock.model';
import { config } from 'src/utils';

@Injectable()
export class StockRepository {
    private stock: Model<any>;

    constructor() {
        this.stock = getModelForClass(Stock);
    }

    async createStock(stockDTO: StockDTO): Promise<Stock> {
        const response = await this.stock.create(stockDTO);

        return response.toJSON();
    }

    async findStockById(id: string): Promise<Stock> {
        const response = await this.stock.findById(id);

        return response;
    }

    async findStockByProductId(id: string): Promise<Stock> {
        const response = await this.stock
            .find({ 'product._id': id })
            .lean();

        return response[0] as Stock;
    }

    async listStock(queryDTO: QueryDTO): Promise<Stock[]> {
        const response = await this.stock
            .find({})
            .skip(Number((queryDTO.page - 1) * queryDTO.per) || config.page)
            .limit(Number(queryDTO.per) || config.per)
            .sort({ 'lastUpdatedAt': Number(queryDTO.sort) || config.sort })
            .lean();

        return response as Stock[];
    }

    async updateStock(stockDTO: StockDTO): Promise<Stock> {
        const response = await this.stock.findOneAndUpdate(
            { _id: stockDTO._id },
            { $set: stockDTO },
            { new: true }
        );

        return response;
    }
}
