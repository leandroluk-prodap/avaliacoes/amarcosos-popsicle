import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { getModelForClass } from '@typegoose/typegoose';

import { QueryDTO } from 'src/dtos/query.dto';
import { SiteDTO } from 'src/dtos/site.dto';
import { Site } from 'src/models/site.model';
import { config } from 'src/utils';

@Injectable()
export class SiteRepository {
    private site: Model<any>;

    constructor() {
        this.site = getModelForClass(Site);
    }

    async createSite(siteDTO: SiteDTO): Promise<Site> {
        const response = await this.site.create(siteDTO);

        return response.toJSON();
    }

    async findSiteById(id: string): Promise<Site> {
        const response = await this.site.findById(id);

        return response;
    }

    async listSite(queryDTO: QueryDTO): Promise<Site[]> {
        const response = await this.site
            .find({})
            .skip(Number((queryDTO.page - 1) * queryDTO.per) || config.page)
            .limit(Number(queryDTO.per) || config.per)
            .sort({ 'lastUpdatedAt': Number(queryDTO.sort) || config.sort })
            .lean();

        return response as [];
    }
}
