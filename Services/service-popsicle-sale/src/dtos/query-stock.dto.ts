import {
    IsMongoId,
    IsNumberString,
    IsOptional
} from 'class-validator';

export class QueryStockDTO {
    @IsMongoId()
    @IsOptional()
    stockId: string;

    @IsNumberString()
    @IsOptional()
    operationQuantity: number;

    @IsMongoId()
    @IsOptional()
    salesmanId: string;
}
