import {
    IsArray,
    IsBoolean,
    IsDate,
    IsMongoId,
    IsNumber,
    IsOptional,
    IsString
} from 'class-validator';
import { ObjectId } from 'mongodb';

import { ProductType } from '../models/product.model';

export class ProductDTO {
    @IsMongoId()
    @IsOptional()
    _id?: ObjectId;

    @IsBoolean()
    @IsOptional()
    active?: boolean;

    @IsDate()
    @IsOptional()
    createdAt?: Date;

    @IsString()
    @IsOptional()
    description: string;

    @IsArray()
    @IsOptional()
    images?: string[];

    @IsDate()
    @IsOptional()
    lastUpdatedAt?: Date;

    @IsNumber()
    @IsOptional()
    price?: number;

    @IsNumber()
    @IsOptional()
    promotionPrice: number;

    @IsString()
    @IsOptional()
    type: ProductType;

    @IsNumber()
    @IsOptional()
    validityOutOfTheFreezerInHours?: number;
}
