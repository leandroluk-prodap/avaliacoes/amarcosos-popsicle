import {
    IsMongoId,
    IsNumber,
    IsOptional,
    IsString,
    Max,
    Min
} from 'class-validator';
import { Type } from 'class-transformer';

export class QueryDTO {
    @IsNumber()
    @IsOptional()
    @Min(1)
    @Type(() => Number)
    page?: number;

    @IsNumber()
    @IsOptional()
    @Min(1)
    @Type(() => Number)
    per?: number;

    @IsMongoId()
    @IsOptional()
    salesmanId?: string;

    @IsString()
    @IsOptional()
    search?: string;

    @IsNumber()
    @IsOptional()
    @Min(-1)
    @Max(1)
    @Type(() => Number)
    sort?: number = -1;
}
