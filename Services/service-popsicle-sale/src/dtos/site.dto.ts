import {
    IsBoolean,
    IsDate,
    IsMongoId,
    IsNumber,
    IsObject,
    IsOptional,
    IsString
} from 'class-validator';
import { ObjectId } from 'mongodb';

export class StreetAddressDTO {
    @IsString()
    @IsOptional()
    complement?: string;

    @IsString()
    @IsOptional()
    name: string;

    @IsNumber()
    @IsOptional()
    number?: number;
}

export class SiteAddressDTO {
    @IsString()
    @IsOptional()
    city: string;

    @IsNumber()
    @IsOptional()
    cityId?: number;

    @IsString()
    @IsOptional()
    country: string;

    @IsNumber()
    @IsOptional()
    countryId?: number;

    @IsString()
    @IsOptional()
    state: string;

    @IsNumber()
    @IsOptional()
    stateId?: number;

    @IsObject()
    @IsOptional()
    street?: StreetAddressDTO;
}

export class SiteDTO {
    @IsMongoId()
    @IsOptional()
    _id?: ObjectId;

    @IsBoolean()
    @IsOptional()
    active?: boolean;

    @IsObject()
    @IsOptional()
    address?: SiteAddressDTO;

    @IsDate()
    @IsOptional()
    createdAt?: Date;

    @IsDate()
    @IsOptional()
    lastUpdatedAt?: Date;

    @IsString()
    @IsOptional()
    name: string;
}
