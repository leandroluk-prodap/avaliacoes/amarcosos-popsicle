import {
    IsBoolean,
    IsDate,
    IsMongoId,
    IsObject,
    IsOptional,
    IsString
} from 'class-validator';
import { ObjectId } from 'mongodb';

import { SiteDTO } from './site.dto';

export class SalesmanDTO {
    @IsMongoId()
    @IsOptional()
    _id?: ObjectId;

    @IsBoolean()
    @IsOptional()
    active?: boolean;

    @IsString()
    @IsOptional()
    cpf?: string;

    @IsDate()
    @IsOptional()
    createdAt?: Date;

    @IsDate()
    @IsOptional()
    lastUpdatedAt?: Date;

    @IsString()
    @IsOptional()
    name?: string;

    @IsString()
    @IsOptional()
    photo?: string;

    @IsObject()
    @IsOptional()
    site?: SiteDTO;
}
