import {
    IsDate,
    IsMongoId,
    IsNumber,
    IsObject,
    IsOptional
} from 'class-validator';
import { ObjectId } from 'mongodb';

import { ProductDTO } from './product.dto';
import { SalesmanDTO } from './salesman.dto';
import { SiteDTO } from './site.dto';

export class SaleDTO {
    @IsMongoId()
    @IsOptional()
    _id?: ObjectId;

    @IsDate()
    @IsOptional()
    dateOfSale?: Date;

    @IsObject()
    @IsOptional()
    product: ProductDTO;

    @IsObject()
    @IsOptional()
    salesman: SalesmanDTO;

    @IsNumber()
    @IsOptional()
    quantity: number;

    @IsObject()
    @IsOptional()
    site: SiteDTO;
}
