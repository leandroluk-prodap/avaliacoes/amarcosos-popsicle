import {
    IsArray,
    IsDate,
    IsMongoId,
    IsNumber,
    IsObject,
    IsOptional
} from 'class-validator';
import { ObjectId } from 'mongodb';

import { ProductDTO } from './product.dto';
import { SalesmanDTO } from './salesman.dto';
import { SiteDTO } from './site.dto';

export class UpdatedByDTO {
    @IsMongoId()
    @IsOptional()
    _id?: ObjectId;

    @IsDate()
    @IsOptional()
    date?: Date;

    @IsNumber()
    @IsOptional()
    operationQuantity?: number;

    @IsNumber()
    @IsOptional()
    posteriorQuantity?: number;

    @IsNumber()
    @IsOptional()
    previousQuantity?: number;

    @IsObject()
    @IsOptional()
    salesman?: SalesmanDTO;
}

export class StockDTO {
    @IsMongoId()
    @IsOptional()
    _id?: ObjectId;

    @IsDate()
    @IsOptional()
    createdAt?: Date;

    @IsNumber()
    @IsOptional()
    currentQuantity?: number;

    @IsDate()
    @IsOptional()
    lastUpdatedAt?: Date;

    @IsArray()
    @IsOptional()
    includedBy?: UpdatedByDTO[];

    @IsArray()
    @IsOptional()
    outputBy?: UpdatedByDTO[];

    @IsObject()
    @IsOptional()
    product?: ProductDTO;

    @IsObject()
    @IsOptional()
    site?: SiteDTO;

    @IsNumber()
    @IsOptional()
    startingQuantity?: number;
}
