import { NestFactory } from '@nestjs/core';

import { AppModule } from './app.module';
import { appMiddleware } from './middleware/app.middleware';
import packageJson from '../package.json';

export async function bootstrap(port: number) {
    const app = await NestFactory.create(AppModule);

    appMiddleware(
        app,
        {
            title: 'Popsicle sale service',
            description: packageJson.description,
            version: packageJson.version,
            tag: 'Sale'
        }
    );

    await app.listen(port);
    console.warn(`Express server listening on port ${port}`);
}
